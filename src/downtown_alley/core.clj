(ns downtown-alley.core
  (:require [net.cgrand.enlive-html :as html])
  (:use clojure.pprint)
  (:gen-class))

(defn construct-url [query]
  (str "https://culinaryagents.com/search/jobs?utf8=%E2%9C%93&search%5Bname%5D="
          (java.net.URLEncoder/encode query "UTF-8")
          "&search%5Blocation%5D=New+York%2C+NY&search%5Bcountry%5D=US&refined=&search%5Bcategory%5D=&search%5Btype%5D=&search%5Bradius%5D=20")
  )

(defn construct-url-inputstream [query]
  (with-open [inputstream (-> (java.net.URL. query)
                              .openConnection
                              (doto (.setRequestProperty "User-Agent"
                                                         "Mozilla/5.0 ..."))
                              .getContent)]
    (html/html-resource inputstream)))

(defn construct-full-url [scrape-map]
  (map #( str "https://culinaryagents.com" %) scrape-map))

(defn scrape-hrefs [input-stream]
  (mapcat #(html/attr-values % :href) (html/select input-stream [:div.c9.l8 (html/attr? :href)])))

(defn get-url-list [query]
  (construct-full-url (scrape-hrefs (construct-url-inputstream (construct-url query)))))

(defn print-title-and-description [scrape-map]
  (map (fn [[job-title company location description]] (hash-map :Jobtitle job-title :Company company :Location location :Description description))
                    (partition 4 scrape-map)))

(defn scrape-each-job [input-stream]
  (map html/text (html/select input-stream #{[:div.meta :h2] ;Jobtitle
                                              [:div.meta :h3 :a] ;Company
                                              [:div.job-meta-content :p] ;Location (still needs parsing)
                                              [:div.l8.main-left [(html/nth-of-type 4) :section]]}))) ;Description

(defn print-each-job [url]
  (print-title-and-description (scrape-each-job (construct-url-inputstream url))))

(defn print-jobs [query]
  (print (map print-each-job (get-url-list query))))

(defn -main
  "Scrape CulinaryAgents.com with argument as search term in New York NY"
  [& args]
  (println args)
  (print-jobs (nth args 0)))

